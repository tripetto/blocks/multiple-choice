/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

import {
    ConditionBlock,
    condition,
    isBoolean,
    tripetto,
} from "@tripetto/runner";
import { IChoiceCondition } from "../interface";

@tripetto({
    type: "condition",
    legacyBlock: true,
    identifier: PACKAGE_NAME,
    alias: "multiple-choice",
})
export class MultipleChoiceCondition extends ConditionBlock<IChoiceCondition> {
    @condition
    isChosen(): boolean {
        const multipleChoiceSlot =
            this.valueOf<boolean>(this.props.choice) ||
            this.valueOf<string>("choice");

        if (multipleChoiceSlot) {
            if (isBoolean(multipleChoiceSlot.value)) {
                return multipleChoiceSlot.value;
            }

            return multipleChoiceSlot.reference === this.props.choice;
        }

        return false;
    }
}

export interface IChoice {
    readonly id: string;
    readonly name: string;
    readonly description?: string;
    readonly url?: string;
    readonly target?: "self" | "blank";
    readonly value?: string;
    readonly score?: number;
    readonly exclusive?: boolean;
    readonly color?: string;
}

export interface IMultipleChoice {
    readonly caption?: string;
    readonly imageURL?: string;
    readonly imageWidth?: string;
    readonly imageAboveText?: boolean;
    readonly choices?: IChoice[];
    readonly multiple?: boolean;
    readonly min?: number;
    readonly max?: number;
    readonly alignment?: boolean | "equal" | "full" | "columns";
    readonly required?: boolean;
    readonly randomize?: boolean;
}

export interface IChoiceCondition {
    readonly choice: string | undefined;
}

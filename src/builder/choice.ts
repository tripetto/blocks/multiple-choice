/** Dependencies */
import {
    Collection,
    Components,
    Forms,
    Markdown,
    REGEX_IS_URL,
    Slots,
    affects,
    alias,
    created,
    definition,
    deleted,
    editor,
    insertVariable,
    isBoolean,
    isString,
    markdownifyToString,
    name,
    pgettext,
    refreshed,
    renamed,
    reordered,
    score,
} from "@tripetto/builder";
import { MultipleChoice } from "./";

export class Choice extends Collection.Item<MultipleChoice> {
    @definition("string")
    @name
    name = "";

    @definition("string", "optional")
    description?: string;

    @definition("string", "optional")
    @affects("#refresh")
    url?: string;

    @definition("string", "optional")
    target?: "self" | "blank";

    @definition("string", "optional")
    @affects("#refresh")
    moniker?: string;

    @definition("string", "optional")
    @alias
    value?: string;

    @definition("string", "optional")
    @affects("#refresh")
    labelForTrue?: string;

    @definition("string", "optional")
    @affects("#refresh")
    labelForFalse?: string;

    @definition("number", "optional")
    @score
    score?: number;

    @definition("boolean", "optional")
    @affects("#name")
    exclusive?: boolean;

    @definition("string", "optional")
    color?: string;

    @created
    @reordered
    @renamed
    @refreshed
    defineSlot(): void {
        if (this.ref.multiple && !isString(this.url)) {
            const choiceName =
                (this.name &&
                    markdownifyToString(
                        this.name,
                        Markdown.MarkdownFeatures.Formatting |
                            Markdown.MarkdownFeatures.Hyperlinks
                    )) ||
                undefined;

            const slot = this.ref.slots.dynamic({
                type: Slots.Boolean,
                reference: this.id,
                label: pgettext("block:multiple-choice", "Choice"),
                sequence: this.index,
                name: choiceName,
                alias: this.value,
                required: this.ref.required,
                exportable:
                    this.ref.format !== "concatenate" && this.ref.exportable,
                pipeable: {
                    label: pgettext("block:multiple-choice", "Choice"),
                    content: this.moniker
                        ? {
                              string: choiceName || "",
                              markdown: this.moniker,
                          }
                        : this.name !== choiceName
                        ? {
                              string: choiceName || "",
                              markdown: this.name,
                          }
                        : "name",
                    alias: this.ref.alias,
                    legacy: "Choice",
                },
            });

            slot.labelForTrue =
                this.labelForTrue ||
                this.ref.labelForTrue ||
                pgettext("block:multiple-choice", "Selected");

            slot.labelForFalse =
                this.labelForFalse ||
                this.ref.labelForFalse ||
                pgettext("block:multiple-choice", "Not selected");
        } else {
            this.deleteSlot();
        }
    }

    @deleted
    deleteSlot(): void {
        this.ref.slots.delete(this.id, "dynamic");
    }

    @editor
    defineEditor(): void {
        this.editor.option({
            name: pgettext("block:multiple-choice", "Name"),
            form: {
                title: pgettext("block:multiple-choice", "Choice name"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "name", "")
                    )
                        .action("@", insertVariable(this))
                        .autoFocus()
                        .autoSelect()
                        .enter(this.editor.close)
                        .escape(this.editor.close),
                ],
            },
            locked: true,
        });

        this.editor.option({
            name: pgettext("block:multiple-choice", "Description"),
            form: {
                title: pgettext("block:multiple-choice", "Choice description"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "description", undefined)
                    ).action("@", insertVariable(this)),
                ],
            },
            activated: isString(this.description),
        });

        this.editor.group(pgettext("block:multiple-choice", "Options"));

        const targetCheckbox = new Forms.Checkbox(
            pgettext("block:multiple-choice", "Open in new tab/window"),
            this.target !== "self"
        ).on((target) => {
            this.target =
                target.isFeatureEnabled && !target.isDisabled
                    ? target.isChecked
                        ? "blank"
                        : "self"
                    : undefined;
        });

        this.editor.option({
            name: pgettext("block:multiple-choice", "URL"),
            form: {
                title: pgettext("block:multiple-choice", "URL"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "url", undefined)
                    )
                        .placeholder("https://")
                        .action("@", insertVariable(this))
                        .autoValidate((url) =>
                            url.value === ""
                                ? "unknown"
                                : REGEX_IS_URL.test(url.value) ||
                                  ((url.value.indexOf("mailto:") === 0 ||
                                      url.value.indexOf("tel:") === 0) &&
                                      url.value.length > 10)
                                ? "pass"
                                : "fail"
                        )
                        .on((url) => {
                            targetCheckbox.isDisabled =
                                url.value.indexOf("mailto:") === 0 ||
                                url.value.indexOf("tel:") === 0;
                        }),
                    targetCheckbox,
                    new Forms.Static(
                        pgettext(
                            "block:multiple-choice",
                            "If a URL is set, clicking the choice will open it. The choice cannot be selected as answer."
                        )
                    ),
                ],
            },
            activated: isString(this.url),
            on: (urlFeature: Components.Feature<Forms.Form>) => {
                monikerFeature.disabled(urlFeature.isActivated);
                identifierFeature.disabled(urlFeature.isActivated);
                exclusivityFeature.disabled(
                    urlFeature.isActivated || !this.ref.multiple
                );
                labelsFeature.disabled(
                    urlFeature.isActivated || !this.ref.multiple
                );
            },
        });

        const monikerFeature = this.editor.option({
            name: pgettext("block:multiple-choice", "Moniker"),
            form: {
                title: pgettext("block:multiple-choice", "Choice moniker"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "moniker", undefined)
                    ).action("@", insertVariable(this)),
                    new Forms.Static(
                        pgettext(
                            "block:multiple-choice",
                            "If a moniker is set, this moniker will be used when referred to this choice."
                        )
                    ),
                ],
            },
            activated: isString(this.moniker),
            disabled: isString(this.url),
        });

        this.editor.option({
            name: pgettext("block:multiple-choice", "Color"),
            form: {
                title: pgettext("block:multiple-choice", "Choice color"),
                controls: [
                    new Forms.ColorPicker(
                        Forms.ColorPicker.bind(this, "color", undefined)
                    )
                        .placeholder(
                            pgettext(
                                "block:multiple-choice",
                                "Select an alternative color for this choice"
                            )
                        )
                        .swatches(false, true),
                    new Forms.Static(
                        pgettext(
                            "block:multiple-choice",
                            "Here you can specify an alternative color for the choice button."
                        )
                    ),
                ],
            },
            activated: isString(this.color),
        });

        const exclusivityFeature = this.editor.option({
            name: pgettext("block:multiple-choice", "Exclusivity"),
            form: {
                title: pgettext("block:multiple-choice", "Choice exclusivity"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:multiple-choice",
                            "Unselect all other selected choices when selected"
                        ),
                        Forms.Checkbox.bind(this, "exclusive", undefined, true)
                    ),
                ],
            },
            activated:
                (this.ref.multiple &&
                    !isString(this.url) &&
                    isBoolean(this.exclusive)) ||
                false,
            disabled: !this.ref.multiple || isString(this.url),
        });

        const defaultLabelForTrue = pgettext(
            "block:multiple-choice",
            "Selected"
        );
        const defaultLabelForFalse = pgettext(
            "block:multiple-choice",
            "Not selected"
        );

        const labelsFeature = this.editor.option({
            name: pgettext("block:multiple-choice", "Labels"),
            form: {
                title: pgettext("block:multiple-choice", "Labels"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "labelForTrue", undefined)
                    ).placeholder(defaultLabelForTrue),
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "labelForFalse", undefined)
                    ).placeholder(defaultLabelForFalse),
                    new Forms.Static(
                        pgettext(
                            "block:multiple-choice",
                            "These labels will be used in the dataset and override the default values %1 and %2.",
                            `**${defaultLabelForTrue}**`,
                            `**${defaultLabelForFalse}**`
                        )
                    ).markdown(),
                ],
            },
            activated:
                isString(this.labelForTrue) || isString(this.labelForFalse),
            disabled: !this.ref.multiple || isString(this.url),
        });

        const identifierFeature = this.editor.option({
            name: pgettext("block:multiple-choice", "Identifier"),
            form: {
                title: pgettext("block:multiple-choice", "Choice identifier"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "value", undefined)
                    ),
                    new Forms.Static(
                        pgettext(
                            "block:multiple-choice",
                            "If a choice identifier is set, this identifier will be used instead of the label."
                        )
                    ),
                ],
            },
            activated: isString(this.value),
            disabled: isString(this.url),
        });

        const scoreSlot = this.ref.slots.select<Slots.Numeric>(
            "score",
            "feature"
        );

        this.editor.option({
            name: pgettext("block:multiple-choice", "Score"),
            form: {
                title: pgettext("block:multiple-choice", "Score"),
                controls: [
                    new Forms.Numeric(
                        Forms.Numeric.bind(this, "score", undefined)
                    )
                        .precision(scoreSlot?.precision || 0)
                        .digits(scoreSlot?.digits || 0)
                        .decimalSign(scoreSlot?.decimal || "")
                        .thousands(
                            scoreSlot?.separator ? true : false,
                            scoreSlot?.separator || ""
                        )
                        .prefix(scoreSlot?.prefix || "")
                        .prefixPlural(scoreSlot?.prefixPlural || undefined)
                        .suffix(scoreSlot?.suffix || "")
                        .suffixPlural(scoreSlot?.suffixPlural || undefined),
                ],
            },
            activated: true,
            locked: scoreSlot ? true : false,
            disabled: scoreSlot ? false : true,
        });
    }
}

# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: support@tripetto.com\n"
"POT-Creation-Date: 2022-02-15 08:41+0100\n"
"PO-Revision-Date: 2022-04-26 12:21+0200\n"
"Last-Translator: Julian Frauenholz <frauenholz@amicaldo.de>\n"
"Language-Team: \n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.0.1\n"

msgctxt "block:multiple-choice"
msgid "%2 (%1 option)"
msgid_plural "%2 (%1 options)"
msgstr[0] "%2 (%1 Option)"
msgstr[1] "%2 (%1 Optionen)"

msgctxt "block:multiple-choice"
msgid "Alignment"
msgstr "Ausrichtung"

msgctxt "block:multiple-choice"
msgid ""
"All the selected choices are concatenated to a single string of text "
"separated using a configurable separator."
msgstr ""
"Alle ausgewählten Optionen werden zu einer einzigen Textzeichenfolge "
"verkettet, die durch ein konfigurierbares Trennzeichen getrennt ist."

msgctxt "block:multiple-choice"
msgid "Allow the selection of multiple choices"
msgstr "Erlauben Sie die Auswahl von mehreren Optionen"

msgctxt "block:multiple-choice"
msgid "And"
msgstr "Und"

msgctxt "block:multiple-choice"
msgid "Both options above"
msgstr "Beide Optionen oben"

msgctxt "block:multiple-choice"
msgid "Bulleted list"
msgstr "Aufgelistete Liste"

msgctxt "block:multiple-choice"
msgid "Caption"
msgstr "Beschriftungstext"

msgctxt "block:multiple-choice"
msgid "Choice"
msgstr "Auswahl"

msgctxt "block:multiple-choice"
msgid "Choice description"
msgstr "Beschreibung der Auswahl"

msgctxt "block:multiple-choice"
msgid "Choice exclusivity"
msgstr "Ausschließlichkeit der Auswahl"

msgctxt "block:multiple-choice"
msgid "Choice identifier"
msgstr "Bezeichner der Auswahl"

msgctxt "block:multiple-choice"
msgid "Choice moniker"
msgstr "Spitzname der Auswahl"

msgctxt "block:multiple-choice"
msgid "Choice name"
msgstr "Name der Auswahl"

msgctxt "block:multiple-choice"
msgid "Choices"
msgstr "Auswahlmöglichkeiten"

msgctxt "block:multiple-choice"
msgid "Click the + button to add a choice..."
msgstr "Klicken Sie auf die Schaltfläche +, um eine Auswahl hinzuzufügen..."

msgctxt "block:multiple-choice"
msgid "Comma separated"
msgstr "Komma getrennt"

msgctxt "block:multiple-choice"
msgid "Compare mode"
msgstr "Vergleichsmodus"

msgctxt "block:multiple-choice"
msgid "Counter"
msgstr "Zähler"

msgctxt "block:multiple-choice"
msgid "Counter is between"
msgstr "Zähler liegt zwischen"

msgctxt "block:multiple-choice"
msgid "Counter is equal to"
msgstr "Zähler ist gleich"

msgctxt "block:multiple-choice"
msgid "Counter is higher than"
msgstr "Zähler ist höher als"

msgctxt "block:multiple-choice"
msgid "Counter is lower than"
msgstr "Zähler ist niedriger als"

msgctxt "block:multiple-choice"
msgid "Counter is not between"
msgstr "Zähler ist nicht zwischen"

msgctxt "block:multiple-choice"
msgid "Counter is not equal to"
msgstr "Zähler ist nicht gleich"

msgctxt "block:multiple-choice"
msgid "Custom separator"
msgstr "Benutzerdefinierter Separator"

msgctxt "block:multiple-choice"
msgid "Data format"
msgstr "Format der Daten"

msgctxt "block:multiple-choice"
msgid "Description"
msgstr "Beschreibung"

msgctxt "block:multiple-choice"
msgid "Display image on top of the paragraph"
msgstr "Bild am Anfang des Absatzes anzeigen"

msgctxt "block:multiple-choice"
msgid "Every choice as a separate field"
msgstr "Jede Auswahl als eigenes Feld"

msgctxt "block:multiple-choice"
msgid "Every choice is included in the dataset as a separate value."
msgstr "Jede Wahl wird als separater Wert in den Datensatz aufgenommen."

msgctxt "block:multiple-choice"
msgid "Exclusive"
msgstr "Exklusiv"

msgctxt "block:multiple-choice"
msgid "Exclusivity"
msgstr "Ausschließlichkeit"

msgctxt "block:multiple-choice"
msgid ""
"Generates a score based on the selected choices. Open the settings panel "
"for each choice to set the individual score for that choice."
msgstr ""
"Erzeugt eine Punktzahl auf der Grundlage der ausgewählten "
"Auswahlmöglichkeiten. Öffnen Sie das Einstellungsfeld für jede Auswahl, um "
"die individuelle Punktzahl für diese Auswahl festzulegen."

msgctxt "block:multiple-choice"
msgid "Horizontally (left-right)"
msgstr "Horizontal (links-rechts)"

msgctxt "block:multiple-choice"
msgid "How to separate the selected choices:"
msgstr "Wie werden die ausgewählten Optionen getrennt:"

msgctxt "block:multiple-choice"
msgid "Identifier"
msgstr "Bezeichner"

msgctxt "block:multiple-choice"
msgid ""
"If a URL is set, clicking the choice will open it. The choice cannot be "
"selected as answer."
msgstr ""
"Wenn eine URL festgelegt ist, wird sie durch Anklicken der Auswahl "
"geöffnet. Die Auswahl kann nicht als Antwort ausgewählt werden."

msgctxt "block:multiple-choice"
msgid ""
"If a choice identifier is set, this identifier will be used instead of the "
"label."
msgstr ""
"Wenn eine Auswahlbezeichnung festgelegt ist, wird diese anstelle der "
"Bezeichnung verwendet."

msgctxt "block:multiple-choice"
msgid ""
"If a moniker is set, this moniker will be used when referred to this choice."
msgstr ""
"Wenn ein Spitzname festgelegt ist, wird dieser Spitzname verwendet, wenn "
"auf diese Auswahl verwiesen wird."

msgctxt "block:multiple-choice"
msgid "If counter equals"
msgstr "Wenn der Zähler gleich"

msgctxt "block:multiple-choice"
msgid "If counter is between"
msgstr "Wenn der Zähler zwischen"

msgctxt "block:multiple-choice"
msgid "If counter is higher than"
msgstr "Wenn der Zähler höher ist als"

msgctxt "block:multiple-choice"
msgid "If counter is lower than"
msgstr "Wenn der Zähler niedriger ist als"

msgctxt "block:multiple-choice"
msgid "If counter is not between"
msgstr "Wenn der Zähler nicht zwischen"

msgctxt "block:multiple-choice"
msgid "If counter not equals"
msgstr "Wenn der Zähler nicht gleich ist"

msgctxt "block:multiple-choice"
msgid "If score equals"
msgstr "Wenn die Punktzahl gleich ist"

msgctxt "block:multiple-choice"
msgid "If score is between"
msgstr "Wenn die Punktzahl zwischen"

msgctxt "block:multiple-choice"
msgid "If score is higher than"
msgstr "Wenn die Punktzahl höher ist als"

msgctxt "block:multiple-choice"
msgid "If score is lower than"
msgstr "Wenn die Punktzahl niedriger ist als"

msgctxt "block:multiple-choice"
msgid "If score is not between"
msgstr "Wenn die Punktzahl nicht zwischen"

msgctxt "block:multiple-choice"
msgid "If score not equals"
msgstr "Wenn die Punktzahl nicht gleich ist"

msgctxt "block:multiple-choice"
msgid "Image"
msgstr "Bild"

msgctxt "block:multiple-choice"
msgid "Image source URL"
msgstr "Bildquelle URL"

msgctxt "block:multiple-choice"
msgid "Image width (optional)"
msgstr "Bildbreite (optional)"

msgctxt "block:multiple-choice"
msgid ""
"Includes every choice in the dataset together with the concatenated text."
msgstr "Enthält jede Auswahl im Datensatz zusammen mit dem verketteten Text."

msgctxt "block:multiple-choice"
msgid "Labels"
msgstr "Beschriftungen"

msgctxt "block:multiple-choice"
msgid "Language sensitive conjunction (_, _, and _)"
msgstr "Sprachsensitive Konjunktionen (_, _ und _)"

msgctxt "block:multiple-choice"
msgid "Language sensitive disjunction (_, _, or _)"
msgstr "Sprachsensitive Disjunktion (_, _ oder _)"

msgctxt "block:multiple-choice"
msgid "Limits"
msgstr "Grenzwerte"

msgctxt "block:multiple-choice"
msgid "List on multiple lines"
msgstr "Liste auf mehreren Zeilen"

msgctxt "block:multiple-choice"
msgid "Maximum number of selected choices"
msgstr "Maximale Anzahl von Auswahlmöglichkeiten"

msgctxt "block:multiple-choice"
msgid "Minimum number of selected choices"
msgstr "Mindestanzahl der ausgewählten Optionen"

msgctxt "block:multiple-choice"
msgid "Moniker"
msgstr "Spitzname"

msgctxt "block:multiple-choice"
msgid "Multiple choice"
msgstr "Mehrfachauswahl"

msgctxt "block:multiple-choice"
msgid "Multiple select"
msgstr "Mehrfachauswahl"

msgctxt "block:multiple-choice"
msgid "Name"
msgstr "Name"

msgctxt "block:multiple-choice"
msgid "No choice made"
msgstr "Keine Wahl getroffen"

msgctxt "block:multiple-choice"
msgid "Not selected"
msgstr "Nicht ausgewählt"

msgctxt "block:multiple-choice"
msgid "Number"
msgstr "Nummer"

msgctxt "block:multiple-choice"
msgid "Numbered list"
msgstr "Nummerierte Liste"

msgctxt "block:multiple-choice"
msgid "Open in new tab/window"
msgstr "In neuem Tab/Fenster öffnen"

msgctxt "block:multiple-choice"
msgid "Options"
msgstr "Optionen"

msgctxt "block:multiple-choice"
msgid "Randomization"
msgstr "Zufällige Reihenfolge"

msgctxt "block:multiple-choice"
msgid "Randomize the choices (using [Fisher-Yates shuffle](%1))"
msgstr ""
"Zufällige Reihenfolge der Auswahlmöglichkeiten (mit [Fisher-Yates-Mischung]"
"(%1))"

msgctxt "block:multiple-choice"
msgid "Score"
msgstr "Punktzahl"

msgctxt "block:multiple-choice"
msgid "Score is between"
msgstr "Die Punktzahl liegt zwischen"

msgctxt "block:multiple-choice"
msgid "Score is calculated"
msgstr "Die Punktzahl wird berechnet"

msgctxt "block:multiple-choice"
msgid "Score is equal to"
msgstr "Die Punktzahl ist gleich"

msgctxt "block:multiple-choice"
msgid "Score is higher than"
msgstr "Die Punktzahl ist höher als"

msgctxt "block:multiple-choice"
msgid "Score is lower than"
msgstr "Die Punktzahl ist niedriger als"

msgctxt "block:multiple-choice"
msgid "Score is not between"
msgstr "Die Punktzahl liegt nicht zwischen"

msgctxt "block:multiple-choice"
msgid "Score is not calculated"
msgstr "Die Punktzahl wird nicht berechnet"

msgctxt "block:multiple-choice"
msgid "Score is not equal to"
msgstr "Die Punktzahl ist nicht gleich"

msgctxt "block:multiple-choice"
msgid "Selected"
msgstr "Ausgewählt"

msgctxt "block:multiple-choice"
msgid "Space separated"
msgstr "Mit Leerzeichen getrennt"

msgctxt "block:multiple-choice"
msgid "Text field with a list of all selected choices"
msgstr "Textfeld mit einer Liste aller ausgewählten Optionen"

msgctxt "block:multiple-choice"
msgid "Text value"
msgstr "Textwert"

msgctxt "block:multiple-choice"
msgid ""
"These labels will be used in the dataset and override the default values %1 "
"and %2."
msgstr ""
"Diese Bezeichnungen werden im Datensatz verwendet und setzen die "
"Standardwerte %1 und %2 außer Kraft."

msgctxt "block:multiple-choice"
msgid "This setting determines how the data is stored in the dataset:"
msgstr ""
"Diese Einstellung legt fest, wie die Daten im Datensatz gespeichert werden:"

msgctxt "block:multiple-choice"
msgid "Type caption text here..."
msgstr "Beschriftungstext hier eingeben..."

msgctxt "block:multiple-choice"
msgid "URL"
msgstr "URL"

msgctxt "block:multiple-choice"
msgid "Unnamed choice"
msgstr "Unbenannte Wahl"

msgctxt "block:multiple-choice"
msgid "Unselect all other selected choices when selected"
msgstr "Alle anderen ausgewählten Optionen abwählen, wenn ausgewählt"

msgctxt "block:multiple-choice"
msgid "Use fixed number"
msgstr "Feste Nummer verwenden"

msgctxt "block:multiple-choice"
msgid "Use this separator:"
msgstr "Verwende dieses Trennzeichen:"

msgctxt "block:multiple-choice"
msgid "Use value of"
msgstr "Verwende den Wert von"

msgctxt "block:multiple-choice"
msgid "Value"
msgstr "Wert"

msgctxt "block:multiple-choice"
msgid "Verify counter"
msgstr "Zähler überprüfen"

msgctxt "block:multiple-choice"
msgid "Verify score"
msgstr "Punktzahl überprüfen"

msgctxt "block:multiple-choice"
msgid "Vertically (top-down)"
msgstr "Vertikal (von oben nach unten)"

msgctxt "block:multiple-choice"
msgid "calculated"
msgstr "berechnet"

msgctxt "block:multiple-choice"
msgid "not calculated"
msgstr "nicht berechnet"

msgctxt "block:multiple-choice"
msgid "or"
msgstr "oder"
